package com.costin.connector;

import com.costin.model.Metric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class KafkaProducer {
    private static final int NR_OF_DUPLICATE_METRICS = 3;

    private final Random rand;
    private final AtomicBoolean overlap;
    private final KafkaTemplate<String, Metric> template;

    @Value("${kafka.topic-name-metrics:metrics}")
    private String topicName;

    @Value("${server.name}")
    private String serverName;

    @Autowired
    public KafkaProducer(KafkaTemplate<String, Metric> template) {
        this.rand = new Random();
        this.overlap = new AtomicBoolean(true);
        this.template = template;
    }

    @Scheduled(fixedDelayString = "1000")
    public void producer() throws InterruptedException {
        if (overlap.getAndSet(false)) {
            List<Metric> metrics = new ArrayList<>();
            for (int i = 0; i < NR_OF_DUPLICATE_METRICS; i++) {
                var metric = createMetric();
                sendMetric(metric);
                metrics.add(metric);
                Thread.sleep(1000);
            }
            metrics.forEach(this::sendMetric);
            return;
        }
        sendMetric(createMetric());
    }

    public Metric createMetric() {
        return new Metric(System.currentTimeMillis(), (long) rand.nextInt(100));
    }

    private ListenableFuture<SendResult<String, Metric>> sendMetric(Metric metric) {
        return template.send(topicName, serverName, metric);
    }

}

