package com.costin.connector;

import com.costin.model.Aggregate;
import com.costin.connector.repository.AggregateRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    private final AggregateRepository aggregateRepository;

    @Autowired
    public KafkaConsumer(AggregateRepository aggregateRepository) {
        this.aggregateRepository = aggregateRepository;
    }

    @KafkaListener(topics = {"${kafka.topic-name-aggregates:aggregates}"})
    public void listen(ConsumerRecord<String, Aggregate> cr) {
        LOGGER.info("Key: {} Value: {}", cr.key(), cr.value());
        var aggregate = cr.value();
        aggregateRepository.save(aggregate);
    }
}
