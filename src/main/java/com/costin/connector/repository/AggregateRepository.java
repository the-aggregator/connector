package com.costin.connector.repository;

import com.costin.model.Aggregate;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AggregateRepository {

    private final Session session;
    private final PreparedStatement insertStatement;

    @Autowired
    public AggregateRepository(Session session) {
        this.session = session;
        this.insertStatement = session.prepare("INSERT INTO aggregate " +
                "(server_name, minute, count, sum, average) " +
                "VALUES (:server_name, :minute, :count, :sum, :average)");
    }

    public void save(Aggregate aggregate) {
        BoundStatement statement = insertStatement.bind(
                aggregate.getServerName(),
                aggregate.getMinute(),
                aggregate.getCount(),
                aggregate.getSum(),
                aggregate.getAverage()
        );
        session.executeAsync(statement);
    }
}
